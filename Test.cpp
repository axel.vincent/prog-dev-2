#include <utility>
#include <sstream>
#include "gtest/gtest.h"
#include "Nombre.hpp"

TEST(TestNombre, TestNombreVide)
{
    Nombre n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombreNul)
{
    Nombre n{0};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombre12345678)
{
    Nombre n{12345678};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreConstructionCopie)
{
    Nombre *n1 = new Nombre(1234567890UL);
    Nombre *n2 = new Nombre(*n1);
    std::ostringstream os1;
    os1 << *n1;
    std::ostringstream os2;
    os2 << *n2;
    EXPECT_EQ(os1.str(), "1234567890");
    EXPECT_EQ(os1.str(), os2.str());
    delete n2;
    std::ostringstream os3;
    os3 << *n1;
    EXPECT_EQ(os3.str(), "1234567890");
}

TEST(TestNombre, TestNombreAffectationCopie)
{
    Nombre n1{12345123451234512345UL};
    Nombre n2;
    n2 = n1;
    std::ostringstream os;
    os << n2;
    EXPECT_EQ(os.str(), "12345123451234512345");
}

TEST(TestNombre, TestLectureGrandNombre)
{
    std::string big{"123456789123456789123456789123456789"};
    std::istringstream in{big};
    Nombre n;
    in >> n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), big);
}

TEST(TestNombre, TestPlus)
{
    Nombre n{123};
    Nombre n2 = n + 335;

    std::ostringstream os;
    os << n2;
    EXPECT_EQ(os.str(), "458");

    n2 += 15;

    std::ostringstream os2;
    os2 << n2;
    EXPECT_EQ(os2.str(), "473");
}

TEST(TestNombre, TestMult)
{
    Nombre n{14};
    Nombre n2 = n * 12;

    std::ostringstream os;
    os << n2;
    EXPECT_EQ(os.str(), "168");

    n2 *= 2;

    std::ostringstream os2;
    os2 << n2;
    EXPECT_EQ(os2.str(), "336");
}

TEST(TestNombre, TestFactorielle123)
{
    std::ostringstream os;
    os << factorielle(123);
    EXPECT_EQ(os.str(), "12146304367025329675766243241881295855454217088483382315328918161829235892362167668831156960612640202170735835221294047782591091570411651472186029519906261646730733907419814952960000000000000000000000000000");
}