#include <iostream>
#include "Test.cpp"
#include "gtest/gtest.h"

using namespace std;

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// Un unsigned char est codé sur 1 octet en C++ alors que les int sont codé sur
// 2 octet. Donc coder le chiffre avec des unsigned char permet de réduire d'un
// facteur 2 la taille utilisé.
// Cela s'explique notamment par le fait que les unsigned int servent à représenter
// un très grand nombre d'entier, alors que les char sont en nombre très limités.