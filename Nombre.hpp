#include <iostream>
using namespace std;

class Nombre
{
public:
    Nombre()
    {
        premier_ = new Chiffre(0);
    };
    Nombre(unsigned long n)
    {
        premier_ = new Chiffre(n);
    };

    Nombre(const Nombre &n)
    {
        premier_ = new Chiffre(*n.premier_);
    }

    Nombre &operator=(const Nombre &n)
    {
        Nombre nb(n);
        swap(nb);
        return *this;
    };

    void swap(Nombre &n)
    {
        std::swap(premier_, n.premier_);
    }

    friend ostream &operator<<(ostream &out, const Nombre &n)
    {
        if (n.premier_ != nullptr)
        {
            n.premier_->display(out);
        };
        return out;
    };

    friend istream &operator>>(istream &in, Nombre &n)
    {
        int iterateur = 0;
        while (in.good())
        {
            int c{in.get()};
            if (std::isdigit(c))
            {
                unsigned int d{static_cast<unsigned int>(c - '0')};

                if (iterateur != 0)
                {
                    n.premier_ = new Chiffre(d, *n.premier_);
                }
                else
                {
                    n.premier_ = new Chiffre(d);
                }
                iterateur = iterateur + 1;
                // d contient le chiffre entre 0 et 9 qui vient d'être lu
                // ... à vous de l'utiliser ...
            }
            else
                break;
        }
        return in;
    };

    Nombre operator+=(const int &chiffre)
    {
        Nombre *output = new Nombre();

        Chiffre newPremier = *premier_ + chiffre;
        premier_ = new Chiffre(newPremier);
        return *this;
    };

    friend Nombre operator+(const Nombre &n, const int &chiffre)
    {
        Nombre *output = new Nombre();

        Chiffre *premier = new Chiffre(*n.premier_);
        Chiffre newPremier = *premier + chiffre;
        *output->premier_ = newPremier;
        return *output;
    };

    Nombre operator*=(const int &chiffre)
    {
        Nombre *output = new Nombre();
        Chiffre newPremier = *premier_ * chiffre;
        premier_ = new Chiffre(newPremier);
        return *this;
    };

    friend Nombre operator*(const Nombre &n, const int &chiffre)
    {
        Nombre *output = new Nombre();

        Chiffre *premier = new Chiffre(*n.premier_);
        Chiffre newPremier = *premier * chiffre;
        *output->premier_ = newPremier;
        return *output;
    };
    ~Nombre()
    {
        delete premier_;
    };

private:
    struct Chiffre
    {
        unsigned int chiffre_; // entre 0 et 9
        Chiffre *suivant_;     // chiffre de poids supérieur ou nullptr
        Chiffre(unsigned long n)
        {
            chiffre_ = n % 10;
            if (n > 9)
            {
                suivant_ = new Chiffre(n / 10);
            }
            else
            {
                suivant_ = nullptr;
            }
        };

        Chiffre(const Chiffre &c)
        {
            chiffre_ = c.chiffre_;
            if (c.suivant_ != nullptr)
            {
                suivant_ = new Chiffre(*c.suivant_);
            }
            else
            {
                suivant_ = nullptr;
            }
        }

        Chiffre(unsigned long n, const Chiffre &c)
        {
            chiffre_ = n % 10;
            if (n > 9)
            {
                suivant_ = new Chiffre(n / 10, c);
            }
            else
            {
                suivant_ = new Chiffre(c);
            }
        }

        void display(ostream &out)
        {
            if (suivant_ != nullptr)
            {
                suivant_->display(out);
            }
            out << chiffre_;
        };

        friend Chiffre operator+(const Chiffre &c1, const int &c2)
        {
            int reste = c2 % 10;
            int quotient = c2 / 10;
            if (quotient == 0 && c1.chiffre_ + reste < 10)
            {
                Chiffre *output = new Chiffre(c1);
                output->chiffre_ = c1.chiffre_ + reste;
                return *output;
            };
            // Init a chiffre non null with a suivant_ not null
            Chiffre *output = new Chiffre(11);

            output->chiffre_ = (c1.chiffre_ + reste) % 10;
            if (c1.suivant_ != nullptr)
            {
                //Chiffre *suivant = new Chiffre(*c1.suivant_);
                output->suivant_ = new Chiffre(*(new Chiffre(*c1.suivant_)) + (quotient + (c1.chiffre_ + reste) / 10));
            }
            else
            {
                Chiffre *suivant = new Chiffre(quotient + (c1.chiffre_ + reste) / 10);
                *output->suivant_ = *suivant;
            }
            return *output;
        }

        friend Chiffre operator*(const Chiffre &c1, const int &c2)
        {
            if (c1.chiffre_ * c2 < 10 && c1.suivant_ == nullptr)
            {
                Chiffre *output = new Chiffre(c1);
                output->chiffre_ = c1.chiffre_ * c2;

                return *output;
            };
            // Init a chiffre non null with a suivant_ not null
            Chiffre *output = new Chiffre(11);
            output->chiffre_ = (c1.chiffre_ * c2) % 10;
            if (c1.suivant_ != nullptr)
            {
                Chiffre *suivant = new Chiffre(*c1.suivant_);
                Chiffre suivantMult = *suivant * c2;
                Chiffre somme = suivantMult + ((c1.chiffre_ * c2) / 10);
                Chiffre *s = new Chiffre(somme);
                *output->suivant_ = *s;
            }
            else
            {
                Chiffre *suivant = new Chiffre((c1.chiffre_ * c2) / 10);
                *output->suivant_ = *suivant;
            }
            return *output;
        }

        ~Chiffre()
        {
            delete suivant_;
        };
    };
    Chiffre *premier_;
};

Nombre factorielle(unsigned int n)
{
    Nombre *output = new Nombre(1);
    for (int i = 2; i <= n; i = i + 1)
    {
        *output *= i;
    }
    return *output;
}
